from __future__ import annotations

from typing import Iterable, Optional

from batterycontroller_types import Charge_Responses
from sila2.client import ClientMetadataInstance, ClientObservableCommandInstance, ClientObservableProperty

class BatteryControllerClient:
    """

    Functionality to both provide information on the battery state and control automatic charging abilities.

    """

    BatteryPercentage: ClientObservableProperty[float]
    """
    The current charge percentage of the battery
    """

    BatteryRemainingTime: ClientObservableProperty[float]
    """
    
            The remaining operation time of the robot given the current battery charge.
        
    """
    def Charge(
        self, *, metadata: Optional[Iterable[ClientMetadataInstance]] = None
    ) -> ClientObservableCommandInstance[None, Charge_Responses]:
        """

        Automatically charges the robot by both moving to its charging location and starting the charging process.

        """
        ...
