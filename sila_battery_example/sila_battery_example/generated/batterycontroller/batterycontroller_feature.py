from os.path import dirname, join

from sila2.framework import Feature

BatteryControllerFeature = Feature(open(join(dirname(__file__), "BatteryController.sila.xml")).read())
