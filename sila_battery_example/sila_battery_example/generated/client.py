from __future__ import annotations

from typing import TYPE_CHECKING

from sila2.client import SilaClient

if TYPE_CHECKING:

    from .batterycontroller import BatteryControllerClient


class Client(SilaClient):

    BatteryController: BatteryControllerClient

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
