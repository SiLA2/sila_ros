from typing import Optional
from uuid import UUID

from sila2.server import SilaServer
from numpy.random import randint
from threading import Thread
import time

from .feature_implementations.batterycontroller_impl import BatteryControllerImpl
from .generated.batterycontroller import BatteryControllerFeature


class Server(SilaServer):
    def __init__(self, server_uuid: Optional[UUID] = None):
        # TODO: fill in your server information
        super().__init__(
            server_name="TODO",
            server_type="TODO",
            server_version="0.1",
            server_description="TODO",
            server_vendor_url="https://gitlab.com/SiLA2/sila_python",
            server_uuid=server_uuid,
        )
        self.stopped = False

        self.batterycontroller = BatteryControllerImpl(self)

        self.set_feature_implementation(BatteryControllerFeature, self.batterycontroller)
        self._registered_callbacks = {}
        
        # every time new data arrives:
        #   self.batterycontroller.update_charge_level(new_charge_level)
        #def random_update():
        #    while True:
        #        self.batterycontroller.update_charge_level(randint(100))
        #        time.sleep(1)
        #t = Thread(target=random_update, daemon=True)
        #t.start()
        
    def register_callback(self, key: str, fct):
        self._registered_callbacks[key] = fct
        
    def call_callback(self, key: str, **kwargs):
        if key in self._registered_callbacks:
            return self._registered_callbacks[key](**kwargs)
        
    def has_callback_registered(self, key: str):
        return key in self._registered_callbacks
"""
    def setMyData(data):
    lock mutex
    set data
    unlock 
    
    
    
"""
