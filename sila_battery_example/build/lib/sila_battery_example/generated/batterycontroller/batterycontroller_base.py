from __future__ import annotations

from abc import ABC, abstractmethod
from queue import Queue
from typing import TYPE_CHECKING, Any, Dict, Optional

from sila2.framework import FullyQualifiedIdentifier
from sila2.server import FeatureImplementationBase, ObservableCommandInstance

from .batterycontroller_types import Charge_Responses

if TYPE_CHECKING:
    from sila2.server import SilaServer


class BatteryControllerBase(FeatureImplementationBase, ABC):

    _BatteryPercentage_producer_queue: Queue[float]

    _BatteryRemainingTime_producer_queue: Queue[float]

    def __init__(self, parent_server: SilaServer):
        """

        Functionality to both provide information on the battery state and control automatic charging abilities.

        """
        super().__init__(parent_server=parent_server)

        self._BatteryPercentage_producer_queue = Queue()

        self._BatteryRemainingTime_producer_queue = Queue()

    def update_BatteryPercentage(self, BatteryPercentage: float, queue: Optional[Queue[float]] = None):
        """
        The current charge percentage of the battery

        This method updates the observable property 'BatteryPercentage'.
        """
        if queue is None:
            queue = self._BatteryPercentage_producer_queue
        queue.put(BatteryPercentage)

    def BatteryPercentage_on_subscription(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> Optional[Queue[float]]:
        """
        The current charge percentage of the battery

        This method is called when a client subscribes to the observable property 'BatteryPercentage'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    def update_BatteryRemainingTime(self, BatteryRemainingTime: float, queue: Optional[Queue[float]] = None):
        """

            The remaining operation time of the robot given the current battery charge.


        This method updates the observable property 'BatteryRemainingTime'.
        """
        if queue is None:
            queue = self._BatteryRemainingTime_producer_queue
        queue.put(BatteryRemainingTime)

    def BatteryRemainingTime_on_subscription(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any]
    ) -> Optional[Queue[float]]:
        """

            The remaining operation time of the robot given the current battery charge.


        This method is called when a client subscribes to the observable property 'BatteryRemainingTime'

        :param metadata: The SiLA Client Metadata attached to the call
        :return: Optional `Queue` that should be used for updating this property.
            If None, the default Queue will be used.
        """
        pass

    @abstractmethod
    def Charge(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> Charge_Responses:
        """

            Automatically charges the robot by both moving to its charging location and starting the charging process.



        :param metadata: The SiLA Client Metadata attached to the call
        :param instance: The command instance, enabling sending status updates to subscribed clients

        """
        pass
