from __future__ import annotations

from typing import Any, Dict

from sila2.framework import FullyQualifiedIdentifier, CommandExecutionStatus
from sila2.server import ObservableCommandInstance
import time
import traceback
from datetime import timedelta

from ..generated.batterycontroller import BatteryControllerBase, Charge_Responses


class BatteryControllerImpl(BatteryControllerBase):
    def __init__(self, parent_server: SilaServer):
        super().__init__(parent_server)
        #self.__battery = Battery()  # ROS action client

        self.update_charge_level(50)  # if the properties stay empty, there will be an error
        self.run_periodically(lambda: print('Piep'), delay_seconds = 30)  # demo
        self.percentage = 0
    
    def update_charge_level(self, charge_level, remaining = 1):
        self.update_BatteryPercentage(charge_level)  # every client that is subscribed is provided with a new charge level
        self.update_BatteryRemainingTime(remaining)
        self.percentage = charge_level

    def Charge(
        self, *, metadata: Dict[FullyQualifiedIdentifier, Any], instance: ObservableCommandInstance
    ) -> Charge_Responses:
        try:
            server = self.parent_server
            #assert self.parent_server.has_callback_registered('charge')
            start_percentage = self.percentage
            start = time.time()
            #call ros charge
            def feedback(data):
                instance.progress = data.percentage
                instance.estimated_remaining_time = timedelta(seconds=data.eta_time)
                
            instance.status = CommandExecutionStatus.running
            success = self.parent_server.call_callback('charge', fn_feedback=feedback).success
            assert success
        except Exception as ex:
            print(ex, traceback.print_exc())
