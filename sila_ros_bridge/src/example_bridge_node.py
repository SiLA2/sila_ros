#!/usr/bin/env python3.8
import sys
print(sys.version)
print(sys.version_info)

import rospy
import actionlib
from sila_battery_example import Server
from ros_battery_simulator.msg import ChargeAction, ChargeFeedback, ChargeGoal, ChargeResult, Status

rospy.init_node('example bridge', anonymous=False)


#PARAMS
def action_callback_charge(fn_feedback):
    rospy.loginfo("Callback trigger charge") 
    client = actionlib.SimpleActionClient('/battery_action_node', ChargeAction)
    # Waits until the action server has started up and started
    # listening for goals.
    rospy.loginfo("Callback wait_for_server charge") 
    client.wait_for_server()
    
    #return client

    # Creates a goal to send to the action server.
    rospy.loginfo("Callback ChargeGoal charge") 
    goal = ChargeGoal(charge_rate_a_h=99.0)

    # Sends the goal to the action server.
    rospy.loginfo("Callback send_goal charge") 
    client.send_goal(goal, done_cb=None, active_cb=None, feedback_cb=fn_feedback)

    # Waits for the server to finish performing the action.
    rospy.loginfo("Callback wait_for_result charge") 
    client.wait_for_result()

    # Prints out the result of executing the action
    rospy.loginfo("Callback get_result charge") 
    return client.get_result()

def topic_callback_status(data):
    #rospy.loginfo("Callback trigger data: %f" % data.percentage) 
    sila_server.batterycontroller.update_charge_level(data.percentage)




sila_server = Server()
sila_server.register_callback("charge", action_callback_charge)

rospy.Subscriber("status", Status, topic_callback_status, queue_size=1)

sila_server.start_insecure("127.0.0.1", 50052)


rospy.spin()
sila_server.stop()



