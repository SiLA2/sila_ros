#!/bin/bash
ip=$(hostname --ip)
export ROS_IP=$ip
export ROS_MASTER_URI="http://$ip:11311"
rm -rf "/tmp/.X"*
rm -rf "/home/sila/.vnc/unix:*"
cat /home/sila/.vnc/xstartup.turbovnc
ls /home/sila/.vnc -al
# Start XVnc/X/Lubunu
chmod -f 777 /tmp/.X11-unix
# From: https://superuser.com/questions/806637/xauth-not-creating-xauthority-file (squashes complaints about .Xauthority)
sudo -u sila -H bash -c "touch /home/sila/.Xauthority"
sudo -u sila -H bash -c "xauth generate :1 . trusted"

su - sila  -c "/opt/TurboVNC/bin/vncserver -SecurityTypes None"

/usr/sbin/sshd -f /etc/ssh/sshd_config

if [ $? -eq 0 ] ; then
    /opt/noVNC/utils/launch.sh --vnc $ip:5901 --cert /home/sila/.self.pem --listen 40001;
fi

