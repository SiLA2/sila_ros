from sila2.client import SilaClient
import time
def observe_command(cmd):
    while not cmd.done:
        if cmd.status is not None:
            print(f"status: {cmd.status.name}, time_remaining: {cmd.estimated_remaining_time}, progress:{cmd.progress}", end="\r")
        time.sleep(.1)
    print(f"\nstatus: {cmd.status.name}, response: {cmd.get_responses()} command took {cmd.lifetime_of_execution}")


# sub = client.BatteryController.BatteryPercentage.subscribe()

client = SilaClient('127.0.0.1', 50052, insecure=True)
cmd = client.BatteryController.Charge()
observe_command(cmd)
