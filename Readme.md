
Implementation was created during the bioSASH3 Hackathon. See Todos and Future for what's next.

# How to compile

The easiest way is to use the Docker image available in the repo.
Check out this repo in the src folder (caktin_ws/src) of the working directory. Compile the code via catkin_make and don't forget to source the devel/setup.bash. You also need to install the sila pkg (every time you change it).
```
cd sila_battery_example
python3.8 -m pip install .
```


# How to run

*It is recommended to use a separate terminal for each command. The sequence must be followed.*

```
roslaunch ros_battery_simulator battery_simulator.launch
roslaunch sila_ros_bridge start.launch
python3.8 sila_test_client.py
```

# ToDo's

1. [ ] – clean up example code
1. [ ] – create some charts for docu
1. [ ] – create a documentation
1. [ ] – install the the sila_server with CMake
1. [ ] – impl. ros service bridge
1. [ ] – sila datastream -> ros topic
1. [ ] – impl. abort behaviour

# Future

1. [ ] – reduction to a skeleton
1. [ ] – python-sila code generator augmentation to generate a default skeleton
